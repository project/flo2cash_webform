<?php

/**
 * @file
 * Sample hooks demonstrating usage in flo2cash webform.
 */


/**
 * Respond to a payment attempt.
 * @param $node
 *   The node of the webform
 * @param $values
 *   Array of submitted values keyed by cid.
 * @param $result
 *   An object containing the flo2cash transaction result.
 */
function hook_flo2cash_webform_payment_attempt ($node, $values, $result) {
}


/**
 * Respond to a succesful payment.
 * @param $node
 *   The node of the webform
 * @param $values
 *   Array of submitted values keyed by cid.
 * @param $result
 *   An object containing the flo2cash transaction result.
 */
function hook_flo2cash_webform_payment_made ($node, $values, $result) {
}
