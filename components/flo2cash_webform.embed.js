"use strict";

(function ($) {

  Drupal.behaviors.flo2cash_webform_embed = {

    flo2cash_registered: false,

    ajax: null,

    attach: function (context, settings) {
      $ ("#f2c-card-number", context).once (function () {

        if (typeof Flo2Cash === "undefined") {
          var token = Drupal.settings.flo2cash_webform.error_token
          $.post ("/flo2cash_api/integration/error/" + token, { "error": "Flo2Cash javascript not loaded." })
          return
        }

        // TODO: we currently hard-code that this webform is AJAX enabled.
        // Try making sure we support preview mode as well.
        var id = $ (".webform-next, .webform-submit").attr ("id")
        if (!id) {
          return
        }
        Drupal.behaviors.flo2cash_webform_embed.ajax = Drupal.ajax[id]

        // As we have no way to deregister callbacks, make sure
        // we register Flo2Cash callbacks only once.
        if (!Drupal.behaviors.flo2cash_webform_embed.flo2cash_registered) {
          Flo2Cash.register("onReady", function (evt) {
            var controls = ["number", "expiry-month", "expiry-year", "cvv", "name"]
            $ (controls).each (function (i, control) {
              Flo2Cash.setStyle(control, Drupal.behaviors.flo2cash_webform_embed.control_style (settings, control))
            })
            // don't focus, could be a more lot stuff on the screen
            //Flo2Cash.setFocus ("number")
          })

          Flo2Cash.register("onFieldChange", function (evt) {
            Drupal.behaviors.donate.tokenization = false
            var control = evt.id
            if (control == "expiryMonth")
              control = "expiry-month"
            else if (control == "expiryYear")
              control = "expiry-year"
            if (evt.type == "blur") {
              if (evt.data.type == "input.empty") {
                $ (".flo2cash-credit-card ." + evt.id).addClass ("placeholder")
              }
              $ (".flo2cash-payment-form .form-item ." + control + " .f2c-error .messages.error").remove()
              $ (".flo2cash-payment-form .form-item." + control).removeClass ("error")
              Flo2Cash.validate (control)
            }
            else {
              if (evt.data.type == "input.empty") {
                $ (".flo2cash-credit-card ." + evt.id).removeClass ("placeholder")
              }
              Flo2Cash.setStyle(control, Drupal.behaviors.flo2cash_webform_embed.control_style (settings, control))
            }
          })

          Flo2Cash.register("onError", function (errors) {
            Drupal.behaviors.donate.tokenization = false
            $ (".flo2cash-payment-form .form-item .f2c-error .messages.error").remove()
            $ (".flo2cash-payment-form .form-item").removeClass("error")
            var append_errors = false
            var send_error_to_backend = false
            $ (errors).each (function (i, c) {
              // Rewrite error messages
              if (c.message == "Credit card number is blank" || c.message == "Credit card number is invalid") {
                c.message = "Please enter a valid card number";
              }
              else if (c.message == "Expiry month is blank" || c.message == "Expiry month is not valid number") {
                c.message = "Please enter a valid expiry month";
              }
              else if (c.message == "Expiry year is blank" || c.message == "Expiry year is not valid number" || c.message == "Expiry year can not be less than current year ") {
                c.message = "Please enter a valid expiry year";
              }
              else if (c.message == "CVV number length is invalid. it should be exactly 3 chars long ") {
                c.message = "Please enter a valid 3 digit CVV number";
              }
              else if (c.message == "Card holder name is Blank") {
                c.message = "Don't forget your name!";
              }
              else if (c.message == "tokenisation failed") {
                c.message = "Payment gateway error, please try again later";
                send_error_to_backend = true;
              }
              else {
                send_error_to_backend = true;
              }
              var control = c.id
              if (control == "number" || control == "expiry-month" || control == "expiry-year"  || control == "cvv") {
                if (append_errors) {
                  $ (".flo2cash-payment-form .form-item." + control + " .f2c-error .messages.error").append ("<br>" + c.message)
                }
                else {
                  $ (".flo2cash-payment-form .form-item." + control + " .f2c-error").append ("<div class='messages error'>" + c.message + "</div>")
                  append_errors = true
                }
              }
              else {
                  $ (".flo2cash-payment-form .form-item." + control + " .f2c-error").append ("<div class='messages error'>" + c.message + "</div>")
              }
              $ (".flo2cash-payment-form .form-item." + control).addClass ("error")
              Flo2Cash.setStyle(control, Drupal.behaviors.flo2cash_webform_embed.error_style (settings, control))
              // Display only one error at a time
              //return false
            })
            Drupal.behaviors.donate_desktop.resize_donate_box_height()

            if (send_error_to_backend) {
              var token = Drupal.settings.flo2cash_webform.error_token
              $.post ("/flo2cash_api/integration/error/" + token, { "error": errors })
            }
          })

          Flo2Cash.register("onTokenisation", function (evt) {
            $ ("#f2c-token").val (evt.token)
            var ajax = Drupal.behaviors.flo2cash_webform_embed.ajax
            // Not sure why we need to do this, but it seems we can get stuck
            // at times, thinking we are ajaxing, but we're not really.
            // So we're assuming that if you get here, we want to proceed.
            ajax.ajaxing = false
            return ajax.eventResponse(ajax.element, evt)
          })

          Drupal.behaviors.flo2cash_webform_embed.flo2cash_registered = true
        }

        // Initialise fields.
        Flo2Cash.initialise(settings.flo2cash_webform.api_key, {
          id: "flo2cash-payment-form",
          tokenField: "f2c-token",
          mode: settings.flo2cash_webform.mode,
          inputFields: {
            number: {
              id: "f2c-card-number",
              style: Drupal.behaviors.flo2cash_webform_embed.control_style (settings, "number"),
              placeholder: settings.flo2cash_webform.number_placeholder
            },
            cvv: {
              id: "f2c-cvv",
              style: Drupal.behaviors.flo2cash_webform_embed.control_style (settings, "cvv"),
              placeholder: settings.flo2cash_webform.cvv_placeholder
            },
            expiryDate: {
              id: "f2c-expiry-date",
              "style-month": Drupal.behaviors.flo2cash_webform_embed.control_style (settings, "expiry-month"),
              "style-year": Drupal.behaviors.flo2cash_webform_embed.control_style (settings, "expiry-year"),
              "placeholder-month": settings.flo2cash_webform.expiry_month_placeholder,
              "placeholder-year": settings.flo2cash_webform.expiry_year_placeholder,
            },
            nameOnCard: {
              id: "f2c-name-on-card",
              style: Drupal.behaviors.flo2cash_webform_embed.control_style (settings, "name"),
              placeholder: settings.flo2cash_webform.name_placeholder
            }
          }
        },
        function (errors) {
          console.log ("flo2cash initialisation error")
          console.log (errors)
          var token = Drupal.settings.flo2cash_webform.error_token
          $.post ("/flo2cash_api/integration/error/" + token, { "error": "Flo2Cash initialisation error." })
        })

        // Override "Next step >" handler, to do flo2cash callback.
        // Only when we have a token will we execute the original
        // click handler.
        if (!settings.flo2cash_webform.disable_get_token) {
          $ (Drupal.behaviors.flo2cash_webform_embed.ajax.element).off("click")
          // Don't use context, it will not bind properly
          $ (Drupal.behaviors.flo2cash_webform_embed.ajax.element).on ("click", Drupal.behaviors.flo2cash_webform_embed.validate_credit_card_payment)
        }
      })
    },

    /**
     * If this is a credit card payment, ask flo2cash to validate it.
     */
    validate_credit_card_payment: function (e) {
      try {
        if (Drupal.behaviors.flo2cash_webform_embed.is_credit_card_payment()) {
          if ($ ("#f2c-card-number").length > 0) {
            if (e) {
              e.preventDefault()
            }
            // Calling getToken will validate all inputs, and only proceed
            // if there are no errors.
            Flo2Cash.getToken()
          }
        }
        else {
          if (e) {
            var ajax = Drupal.behaviors.flo2cash_webform_embed.ajax
            return ajax.eventResponse(ajax.element, e)
          }
        }
      }
      catch (err) {
        console.log (err)
      }
    },

    is_credit_card_payment: function () {
      return $("input[name='submitted[page_2][payment_option][payment_type]']:checked").val() === 'credit_card'
    },

    control_style: function (settings, control) {
      var common_style = settings.flo2cash_webform.common_style
      var extra_style = ""
      switch (control) {
        case "number":
          extra_style = settings.flo2cash_webform.number_style
          break
        case "expiry-month":
        case "expiry-year":
          extra_style = settings.flo2cash_webform.expiry_date_style
          break
        case "cvv":
          extra_style = settings.flo2cash_webform.cvv_style
          break
        case "name":
          extra_style = settings.flo2cash_webform.name_style
          break
      }
      return common_style + ";" + extra_style
    },

    error_style: function (settings, control) {
      var error_style = settings.flo2cash_webform.error_style
      return Drupal.behaviors.flo2cash_webform_embed.control_style (settings, control) + ";" + error_style
    }

  }

})(jQuery)
