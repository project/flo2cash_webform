<div class="form-item form-type-textfield number">
  <label for="f2c-card-number">Card Number</label><div id="f2c-card-number"></div>
  <div class="f2c-error" style="display: none;"></div>
  <div class="description">Mastercard/VISA only.</div>
</div>
<div class="form-item form-type-textfield expiry-year expiry-month">
  <label for="f2c-expiry-date">Expiration Date (MM/YY)</label><div id="f2c-expiry-date"></div>
  <div class="f2c-error" style="display: none;"></div>
</div>
<div class="form-item form-type-textfield cvv">
  <label for="f2c-cvv">CVV</label><div id="f2c-cvv"></div>
  <div class="f2c-error" style="display: none;"></div>
</div>
<div class="form-item form-type-textfield name">
  <label for="f2c-name-on-card">Name on card</label><div id="f2c-name-on-card"></div>
  <div class="f2c-error" style="display: none;"></div>
</div>
