<?php

/**
 * @file
 * Webform module flo2cash credit card hosted on site.
 * Card will be sent to your site, but will never be stored.
 */


/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_flo2cash_hosted() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'required' => TRUE,
    'extra' => array(
      'title_display' => FALSE,
      'description' => '',
      'private' => TRUE,
      'analysis' => FALSE,
      'amount' => '[amount]',
      'reference' => variable_get('site_name', ''),
      'particular' => '',
    ),
  );
}


/**
 * Implements _webform_edit_component().
 */
function _webform_edit_flo2cash_hosted($component) {
  $form = array();
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $component['extra']['amount'],
    '#description' => t('The amount to pay. Use special value "[amount]" to select the value of the component of the component with key "amount".'),
    '#size' => 32,
    '#parents' => array('extra', 'amount'),
  );
  $form['reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference'),
    '#default_value' => $component['extra']['reference'],
    '#description' => t('The reference for this payment. Use special value "[reference]" to select the value of the component with key "reference".'),
    '#size' => 32,
    '#parents' => array('extra', 'reference'),
  );
  $form['particular'] = array(
    '#type' => 'textfield',
    '#title' => t('Particular'),
    '#default_value' => $component['extra']['particular'],
    '#description' => t('The particular for this payment. Use special value "[particular]" to select the value of the component with key "particular".'),
    '#size' => 32,
    '#parents' => array('extra', 'particular'),
  );
  return $form;
}


/**
 * Implements _webform_render_component().
 */
function _webform_render_flo2cash_hosted($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'fieldset',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : NULL,
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#description' => $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => array('webform-component-fieldset')),
    '#pre_render' => array('flo2cash_webform_prerender', 'webform_element_title_display'),
    '#process' => array('flo2cash_webform_expand_hosted_credit_card_details'),
    '#supplied_value' => $value,
    '#translatable' => array('title', 'description'),
  );

  if (!empty($value)) {
    $element['#default_value'] = $value;
  }

  return $element;
}


/**
 * Add the actual credit card fields.
 */
function flo2cash_webform_expand_hosted_credit_card_details ($element) {
  $value = isset($element['#default_value']) ? $element['#default_value'] : array (
    'card' => '',
    'expiry_month' => '',
    'expiry_year' => '',
    'csc' => '',
    'card_holder_name' => '',
  );
  $element['card'] = array (
    '#type' => 'textfield',
    '#title' => t ('Card number'),
    '#title_display' => $element['#title_display'],
    '#description' => t ('Only Mastercard/VISA supported.'),
    '#required' => $element['#required'],
    '#size' => 20,
    '#maxlength' => 20,
    '#element_validate' => array ('flo2cash_webform_validate_card'),
    '#default_value' => $value['card'],
    '#attributes' => array (
      'placeholder' => t ('Card number *'),
    ),
    // Webform handles validation manually.
    '#validated' => TRUE,
    '#webform_validated' => FALSE,
  );
  $element['expiry_month'] = array (
    '#type' => 'textfield',
    '#title' => t ('Expiry'),
    '#title_display' => $element['#title_display'],
    '#required' => $element['#required'],
    '#field_prefix' => t ('Month'),
    '#size' => 2,
    '#maxlength' => 2,
    '#element_validate' => array ('flo2cash_webform_validate_month'),
    '#default_value' => $value['expiry_month'],
    '#attributes' => array (
      'placeholder' => t ('MM *'),
    ),
    // Webform handles validation manually.
    '#validated' => TRUE,
    '#webform_validated' => FALSE,
  );
  $element['expiry_year'] = array (
    '#type' => 'textfield',
    '#required' => $element['#required'],
    '#field_prefix' => t ('Year'),
    '#size' => 2,
    '#maxlength' => 2,
    '#element_validate' => array ('flo2cash_webform_validate_year'),
    '#default_value' => $value['expiry_year'],
    '#attributes' => array (
      'placeholder' => t ('YY *'),
    ),
    // Webform handles validation manually.
    '#validated' => TRUE,
    '#webform_validated' => FALSE,
  );
  $element['csc'] = array (
    '#type' => 'textfield',
    '#title' => t ('Security code'),
    '#title_display' => $element['#title_display'],
    '#required' => $element['#required'],
    '#size' => 4,
    '#element_validate' => array ('flo2cash_webform_validate_csc'),
    '#default_value' => $value['csc'],
    '#attributes' => array (
      'placeholder' => t ('Code *'),
    ),
    // Webform handles validation manually.
    '#validated' => TRUE,
    '#webform_validated' => FALSE,
  );
  $element['card_holder_name'] = array (
    '#type' => 'textfield',
    '#title' => t ('Name'),
    '#title_display' => $element['#title_display'],
    '#required' => $element['#required'],
    '#size' => 60,
    '#default_value' => $value['card_holder_name'],
    '#attributes' => array (
      'placeholder' => t ('Name *'),
    ),
    // Webform handles validation manually.
    '#validated' => TRUE,
    '#webform_validated' => FALSE,
  );

  // Add HTML5 required attribute, if needed.
  if ($element['#required']) {
    foreach (element_children ($element) as $key) {
      $element[$key]['#attributes']['required'] = 'required';
    }
  }
  $path = drupal_get_path ('module', 'flo2cash_webform');
  $element['#attached']['css'] = array (
    $path . '/flo2cash_webform.css',
  );
  return $element;
}


function flo2cash_webform_validate_month ($element, &$form_state, $form) {
  $values = $form_state['values']['submitted']['credit_card_details'];
  if (!preg_match ('/^[0-9]{1,2}$/', $values['expiry_month']))
    form_error ($element, t ('Month must be two digits.'));
}


function flo2cash_webform_validate_year ($element, &$form_state, $form) {
  $values = $form_state['values']['submitted']['credit_card_details'];
  if (!preg_match ('/^[0-9]{2}$/', $values['expiry_year']))
    form_error ($element, t ('Year must be two digits.'));
}


function flo2cash_webform_validate_csc ($element, &$form_state, $form) {
  $values = $form_state['values']['submitted']['credit_card_details'];
  if (!preg_match ('/^[0-9]{3,4}$/', $values['csc']))
    form_error ($element, t ('CSC must be a three or 4 digit number.'));
}


/**
 * Implements _webform_submit_component().
 * This is called after our validate/true submit.
 */
function _webform_submit_flo2cash_hosted($component, $value) {
  if (!empty ($value['result']))
    return (array) $value['result'];
  else
    return $value;
}


/**
 * Called manually from the main form validation handler at the last submit.
 */
function _webform_submit_to_flo2cash_flo2cash_hosted ($component, $node, $values) {
  $amount = flo2cash_webform_get_value ($node, $values, $component['extra']['amount']);
  $reference = flo2cash_webform_get_value ($node, $values, $component['extra']['reference']);
  $particular = flo2cash_webform_get_value ($node, $values, $component['extra']['particular']);
  $cc_values = $values[$component['cid']];
  $month = $cc_values['expiry_month'];
  $year = $cc_values['expiry_year'];
  $start_date = '2017-06-01';
  $result = flo2cash_payment_ws_process_purchase ($amount, $reference, $particular, $cc_values['card'], $month, $year, $cc_values['card_holder_name'], $cc_values['csc']);
  return $result;
}
