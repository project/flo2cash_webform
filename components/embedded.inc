<?php

/**
 * @file
 * Webform module flo2cash embedded credit card component.
 * Card will never be seen by your site.
 */


/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_flo2cash_embed() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'required' => TRUE,
    'extra' => array(
      'title_display' => FALSE,
      'description' => '',
      'private' => TRUE,
      'analysis' => FALSE,
      'amount' => '[amount]',
      'reference' => variable_get('site_name', ''),
      'particular' => '',
      'common_style' => 'border: 1px solid black;',
      'number_style' => '',
      'expiry_date_style' => '',
      'cvv_style' => '',
      'name_style' => '',
      'error_style' => '',
      'number_placeholder' => '',
      'expiry_month_placeholder' => '',
      'expiry_year_placeholder' => '',
      'cvv_placeholder' => '',
      'name_placeholder' => '',
      'disable_get_token' => FALSE,
    ),
  );
}


/**
 * Implements _webform_edit_component().
 */
function _webform_edit_flo2cash_embed($component) {
  $form = array();
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $component['extra']['amount'],
    '#description' => t('The amount to pay. Use special value "[amount]" to select the value of the component of the component with key "amount".'),
    '#size' => 32,
    '#parents' => array('extra', 'amount'),
  );
  $form['reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference'),
    '#default_value' => $component['extra']['reference'],
    '#description' => t('The reference for this payment. Use special value "[reference]" to select the value of the component with key "reference".'),
    '#size' => 32,
    '#parents' => array('extra', 'reference'),
  );
  $form['particular'] = array(
    '#type' => 'textfield',
    '#title' => t('Particular'),
    '#default_value' => $component['extra']['particular'],
    '#description' => t('The particular for this payment. Use special value "[particular]" to select the value of the component with key "particular".'),
    '#size' => 32,
    '#parents' => array('extra', 'particular'),
  );
  $form['common_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Common style'),
    '#default_value' => $component['extra']['common_style'],
    '#description' => t('flo2cash displays every credit card field in a separate iframe, with very limited styling options. Supply here the common CSS style to apply to the flo2cash input field.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'common_style'),
  );
  $form['number_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Number style'),
    '#default_value' => $component['extra']['number_style'],
    '#description' => t('Additional styling for the credit card number.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'number_style'),
  );
  $form['expiry_date_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry date style'),
    '#default_value' => $component['extra']['expiry_date_style'],
    '#description' => t('Additional styling for the expiry date.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'expiry_date_style'),
  );
  $form['cvv_style'] = array(
    '#type' => 'textfield',
    '#title' => t('CVV style'),
    '#default_value' => $component['extra']['expiry_date_style'],
    '#description' => t('Additional styling for the CVV.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'cvv_style'),
  );
  $form['name_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Name style'),
    '#default_value' => $component['extra']['name_style'],
    '#description' => t('Additional styling for the name field.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'name_style'),
  );
  $form['error_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Error style'),
    '#default_value' => $component['extra']['error_style'],
    '#description' => t('Supply the style to use when flo2cash reports an error with a field. This style will be added to the existing styling of the input field.'),
    '#size' => 128,
    '#maxlength' => 4096,
    '#parents' => array('extra', 'error_style'),
  );
  $form['number_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Number placeholder'),
    '#default_value' => $component['extra']['number_placeholder'],
    '#description' => t('Optional value of the placeholder attribute.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#parents' => array('extra', 'number_placeholder'),
  );
  $form['expiry_month_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry month placeholder'),
    '#default_value' => $component['extra']['expiry_month_placeholder'],
    '#description' => t('Optional value of the placeholder attribute.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#parents' => array('extra', 'expiry_month_placeholder'),
  );
  $form['expiry_year_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry year placeholder'),
    '#default_value' => $component['extra']['expiry_year_placeholder'],
    '#description' => t('Optional value of the placeholder attribute.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#parents' => array('extra', 'expiry_year_placeholder'),
  );
  $form['cvv_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('CVV placeholder'),
    '#default_value' => $component['extra']['cvv_placeholder'],
    '#description' => t('Optional value of the placeholder attribute.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#parents' => array('extra', 'cvv_placeholder'),
  );
  $form['name_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Name placeholder'),
    '#default_value' => $component['extra']['name_placeholder'],
    '#description' => t('Optional value of the placeholder attribute.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#parents' => array('extra', 'name_placeholder'),
  );
  $form['disable_get_token'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable get token'),
    '#default_value' => $component['extra']['disable_get_token'],
    '#description' => t('External javascript will call flo2cash getToken().'),
    '#parents' => array('extra', 'disable_get_token'),
  );
  return $form;
}


/**
 * Implements _webform_render_component().
 */
function _webform_render_flo2cash_embed($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'fieldset',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : NULL,
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#description' => $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => array(
      'id' => 'flo2cash-payment-form',
      'class' => array('webform-component-fieldset', 'flo2cash-payment-form'),
    ),
    '#pre_render' => array('flo2cash_webform_prerender', 'webform_element_title_display'),
    '#process' => array('flo2cash_webform_expand_embedded_credit_card_details'),
    '#supplied_value' => $value,
    '#translatable' => array('title', 'description'),
  );

  if (!empty($value)) {
    $element['#default_value'] = $value;
  }

  return $element;
}


/**
 * Add the actual credit card fields.
 */
function flo2cash_webform_expand_embedded_credit_card_details ($element) {
  $api_key = flo2cash_integration_api_key();
  drupal_page_is_cacheable(FALSE);
  if ($api_key) {
    $element['f2c_iframe'] = array (
      '#type' => 'markup',
      '#markup' => theme('flo2cash_iframe_credit_card'),
    );
    $element['token'] = array (
      '#type' => 'hidden',
      '#attributes' => array ('id' => 'f2c-token'),
    );

    // Add HTML5 required attribute, if needed.
    if ($element['#required']) {
      foreach (element_children ($element) as $key) {
        $element[$key]['#attributes']['required'] = 'required';
      }
    }
    $path = drupal_get_path ('module', 'flo2cash_webform');
    $element['#attached']['css'] = array (
      $path . '/flo2cash_webform.css',
    );
    $element['#attached']['js'] = array (
      $path . '/components/flo2cash_webform.embed.js',
    );
    $component = $element['#webform_component'];
    $element['#attached']['js'][] = array (
      'type' => 'setting',
      'data' => array (
        'flo2cash_webform' => array (
          'api_key' => $api_key,
          'mode' => variable_get('flo2cash_api_is_demo', FALSE) ? 'sandbox' : 'prod',
          'common_style' => $component['extra']['common_style'],
          'number_style' => $component['extra']['number_style'],
          'number_placeholder' => $component['extra']['number_placeholder'],
          'expiry_date_style' => $component['extra']['expiry_date_style'],
          'expiry_month_placeholder' => $component['extra']['expiry_month_placeholder'],
          'expiry_year_placeholder' => $component['extra']['expiry_year_placeholder'],
          'cvv_style' => $component['extra']['cvv_style'],
          'cvv_placeholder' => $component['extra']['cvv_placeholder'],
          'name_style' => $component['extra']['name_style'],
          'name_placeholder' => $component['extra']['name_placeholder'],
          'error_style' => $component['extra']['error_style'],
          'disable_get_token' => $component['extra']['disable_get_token'],
          'error_token' => drupal_get_token(),
        ),
      ),
    );
  }
  else {
    $element['f2c_no_api_key'] = array (
      '#type' => 'markup',
      '#markup' => '<div id="f2c-no-api-key">' . t ('Apologies, we are unable to take credit cards at this moment.') . '</div>',
    );
  }
  return $element;
}


/**
 * Implements _webform_submit_component().
 * This is called after our validate/true submit.
 */
function _webform_submit_flo2cash_embed($component, $value) {
  return $value;
}


/**
 * Called manually from the main form validation handler at the last submit.
 */
function _webform_submit_to_flo2cash_flo2cash_embed ($component, $node, $values) {
  $amount = flo2cash_webform_get_value ($node, $values, $component['extra']['amount']);
  $reference = flo2cash_webform_get_value ($node, $values, $component['extra']['reference']);
  $particular = flo2cash_webform_get_value ($node, $values, $component['extra']['particular']);
  $cc_values = $values[$component['cid']];
  if (!empty ($cc_values['token'])) {
    $token = $cc_values['token'];
    $result = flo2cash_integration_purchase ($amount, 'NZD', $reference, $particular, $token);
  }
  else {
    $result = (object) array (
      'Status' => 'NO TOKEN',
      'Message' => t ('No token found.'),
      'Amount' => $amount,
      'Reference' => $reference,
      'Particular' => $particular,
    );
    watchdog ('flo2cash_api', 'No token available.', array (), WATCHDOG_CRITICAL);
  }
  return $result;
}
