Description
-----------
Makes a flo2cash credit card details field available. Add this to a
webform to take credit card details, send the supplied credit card to
flo2cash, and store the result of that transaction in the webform.

Note that the credit card submission is done at the validate event, so
any errors in the entered credit card details can be easily corrected
by customer.


Dependencies
------------
Needs the flo2cash_api module.
